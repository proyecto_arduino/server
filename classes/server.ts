import express from 'express';
import { SERVER_PORT } from '../global/enviroment';
import socketIO from 'socket.io';
import http from 'http';
import * as socket from '../socket/socket';
import Serialport from 'serialport';

export default class Server{

    private static _instance: Server;
    public app: express.Application;
    public port: number;
    public io: socketIO.Server;
    private httpServer:http.Server;
    public portArduino:any;
    public parserArduino:any;

    private constructor(){
        this.app = express();
        this.port = SERVER_PORT;
        this.httpServer = new http.Server(this.app);
        this.io = socketIO(this.httpServer);
        this.escucharSocket();
        //Arduino
        const Readline = Serialport.parsers.Readline;
        this.portArduino= new Serialport("COM6", {
            baudRate: 9600
        });
        this.parserArduino = this.portArduino.pipe(new Readline({delimiter: '\r\n'}));

    }

    public static get instance(){
        return this._instance || (this._instance = new this());
    }

    private escucharSocket(){

        this.io.on('connection', cliente =>{
            
            console.log("(Server) Cliente conectado");

            this.parserArduino.on('data', (data:string) => {
                let t =  String(data);
                socket.mensajearduino(cliente, this.io, t);
            });
            
            socket.desconectar(cliente);

        })
    }
    //Metodo para levantar Servidor
    start(callback:Function){
        this.httpServer.listen(this.port, callback());
    }
}