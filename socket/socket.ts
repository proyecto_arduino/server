import { Socket } from 'socket.io';
import socketIO from 'socket.io';

export const desconectar = (cliente: Socket) => {

    cliente.on('disconnect', () =>{
        console.log("(Server) Cliente desconectado");
    })
}

export const mensaje = ( cliente: Socket , io: socketIO.Server ) => {
   
    cliente.on('mensaje', (payload: { cuerpo:string}) =>{

        console.log("mensaje recibido " + JSON.stringify(payload));

       // io.emit('mensaje-nuevo', 'hola desde server')

    }) 
}

export const mensajearduino = ( cliente: Socket , io: socketIO.Server, valor:string ) => {
   
   // cliente.on('mensaje', (payload: { cuerpo:string}) =>{

        io.emit('mensaje-nuevo', valor)

    //}) 
}